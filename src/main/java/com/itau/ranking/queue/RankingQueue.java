package com.itau.ranking.queue;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.JmsMessageHeaderAccessor;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.itau.ranking.models.Ranking;
import com.itau.ranking.repositories.RankingRepository;


@Component
public class RankingQueue {

	@Autowired
	private RankingRepository rankingRepository;

	@JmsListener(destination = "b2.queue.ranking", containerFactory = "rankingJmsFactory")
	public void receiveRequestAllRanking(@Payload Map<String, String> message,
			JmsMessageHeaderAccessor jmsMessageHeaderAccessor) {
		
		Ranking ranking = new Ranking();
		
		ranking.setJogador(message.get("playerName"));
		ranking.setTipoJogo(message.get("gameId"));
		ranking.setAcerto(Integer.parseInt(message.get("hits")));
		ranking.setErro(Integer.parseInt(message.get("misses")));
		ranking.setTotal(Integer.parseInt(message.get("total")));
		rankingRepository.save(ranking);
	}
}
