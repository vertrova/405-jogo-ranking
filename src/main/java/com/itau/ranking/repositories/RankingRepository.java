package com.itau.ranking.repositories;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import com.itau.ranking.models.Ranking;

public interface RankingRepository extends CrudRepository<Ranking,Long> {
	public Optional<Ranking> findTop10ByTipoJogoOrderByAcertoDesc(String tipoJogo);

}