package com.itau.ranking.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itau.ranking.models.Ranking;
import com.itau.ranking.repositories.RankingRepository;

@RestController
public class RankingController {

	
	@Autowired
	RankingRepository rankingRepository;
	
	@RequestMapping(method=RequestMethod.POST,path="/ranking")
	public Ranking inserirRanking(@RequestBody Ranking ranking) {
		
		return rankingRepository.save(ranking);
		
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/rankingGet")
	public Iterable<Ranking> getAllRanking() {
		return rankingRepository.findAll();
	}
	
	@RequestMapping(value = "/api/v1/ranking", method = RequestMethod.GET)
	public ResponseEntity<?> getRanking(@RequestParam("search") String q) {
		Optional<Ranking> searchResults = null;
		
		searchResults = rankingRepository.findTop10ByTipoJogoOrderByAcertoDesc(q);
		
		return ResponseEntity.ok().body(searchResults);
	}
	
}
