package com.itau.ranking.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Ranking {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String tipoJogo;
	private String jogador;
	private int acerto;
	private int erro;
	private int total;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTipoJogo() {
		return tipoJogo;
	}
	public void setTipoJogo(String tipoJogo) {
		this.tipoJogo = tipoJogo;
	}
	public String getJogador() {
		return jogador;
	}
	public void setJogador(String jogador) {
		this.jogador = jogador;
	}
	
	public int getAcerto() {
		return acerto;
	}
	public void setAcerto(int acerto) {
		this.acerto = acerto;
	}
	public int getErro() {
		return erro;
	}
	public void setErro(int erro) {
		this.erro = erro;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
}
